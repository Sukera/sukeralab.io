#! /usr/bin/env julia

for (root, dirs, files) in walkdir(".")
    for f in files
        if occursin(r"\.html$", f) &&
            !(root == "." &&
            f == "template.html")
            
            rm(joinpath(root, f))
        end
    end
end