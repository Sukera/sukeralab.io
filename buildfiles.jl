#! /usr/bin/env julia

i = IOBuffer()

for (root, dirs, files) in walkdir(".")
    pubname = root*"/.published"
    published = isfile(pubname)

    for file in files
        !occursin(r"\.md$", file) && continue

        fname = root*'/'*file
        hname = replace(fname, r"\.md$" => ".html")
        fstat = stat(fname).mtime
        hstat = stat(hname).mtime

        if published
            pubstat = stat(pubname).mtime
            build = pubstat > hstat || fstat > hstat
        else
            build = fstat > hstat
        end
        # @show fname, published, build
        if build
            write(i, escape_string(fname, ' '))
            write(i, ' ')
        end
    end
end

println(String(take!(i)))