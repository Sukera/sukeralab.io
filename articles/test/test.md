# THE Reference

You've found my markdown reference! It's not usually linked from the main site.

I use this file for testing out styles for new parts, seeing how things fit together or how the text renders.

## Emphasis!

For example, **this text** is bold! Likewise, there's *italics* and `inline literals`. There's no underlined text, since this is a website and that style is reserved for links.

## Code!

Markdown has multiple ways of formatting code literals and I can distinguish them with the Julia-builtin Markdown renderer -- single quoted just becomes a `code` element, whereas the triple-quoted version is additionally wrapped in `pre`.

```
This is triple quoted!
```

For big blocks, you can also specify the language it should be highlighted for:

```julia
"""
    But I don't have it working yet :(

    Maybe there's JS for this? I'd rather not use it though. I wonder if this long line will stick out of the text element?
"""
function example()
    println("Hello from Julia!")
end
```

In theory there's math mode, but I couldn't get it to work yet. Maybe in the future :)

!!! note

    There's a bunch more, but you'll have to read on!!

## Lists!

Lists are useful - they can tell you where to look. Like down there:

 * I'm in the top layer!
   * I'm in the second layer.
   * I'm here to fill the void..
 * I'm a second point.
   * I add information to my main point
   * I'm here because people seldom read this far.
     * And even fewer go three nestings deep - sheesh!
 * I'm here for balance!


Some text has to fill this space, so we can see what the list looks like when it's embedded in text.

Other lists are also useful:

 1. They're numbered.
 2. They can provide an ordering
 3. And you instantly know how long this drags on.

## Here's an image

(Pssst! Down here! Want some [kittens](https://lmddgtfy.net/?q=Kittens)?)

![This is Diego the cat!](cat.jpg)

Look at this kitten, ain't it cute[^1]?

## Tableau

| The | Fancy | Table |
|:----|-------|:-----:|
|is|the|best|

These tables sadly don't output semantic HTML, so styling them is difficult. And the user agent styles these weird by default.

!!! warning "Small tables are no joke!"

    If you get stuck with one of these, better think of a new representation..

    This is a warning, by the way -- fancy, no?

## The end?

> A quote is special
> 
> Words, both oldest and youngest
> 
> To be repeated
>
> _A Haiku, by Author_

I need to find a way to mark the author specially since I'd like to preserve emphasis even inside blockquotes.. Sadly, I can't really make use of the `<cite>` tags, since they don't seem to be exposed in markdown. I'd rather not write HTML.

## The end?

Alright, that's about it.

### Yep.

#### Totally.

##### You still here?

###### I haven't styled this far, here be dragons.

You shall not proceed![^2]

---

[^1]: [Thanks to Kim for this picture!](https://www.flickr.com/photos/57873306@N00/195105464)

[^2]: Jokes aside, I don't see myself ever using that hbar element...