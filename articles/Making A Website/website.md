# ..or rather, _this_ website

This isn't going to be like one of those "How it's made" videos you can find all over the internet or an article about how anyone can write a website in X easy steps. This article is serving as documentation mainly from myself to myself on how this site is built at the time of writing before I forget how it works.

## The background

My main goal with this project was to create a place where I can put my thoughts. I wanted it to be portable by some reasonable measure and future proof, if possible. So the stack was clear: The web. HTML & CSS seem to finally be stabilizing with a reasonable feature set, although there are still some things I'd like to use that aren't available yet.

Now, what to build the site with? There's a myriad of static-site generators out there and while I'd love to use one of those, they all seem pretty strict in terms of the layout of the site. I want the most freedom possible to link and design as I see fit, so I decided to build my own. Originally I wanted to implement DOM manipulation using CSS-selectors (inspired by [this]()), but I got stuck halfway through ignoring JavaScript and actually building a tree. Maybe I'll finish it in the future.

## The inner workings

The project is largely driven by a single Makefile which calls out to a few scripts.

 * ./Makefile
   * The Makefile doesn't do much, just calling out to `./articles/articles.jl`, `./convert.jl` and `cleanfiles.jl` to remove all `.html` files on a `clean`. I originally used the shell native `rm`, but that has problems with the spaces in this articles' path.  
 * ./template.html
   * The template dictates the general structure of every page built. It also has some spaces where custom things can be inserted. I've managed to delete this twice already and at the time of writing this, I still haven't put any of this in a git. Whoops. 
 * ./buildfiles.jl
   * Finds all `.md` files in all subdirectories and checks whether it should be rebuilt into a `.html`. This is necessary because `make` can't really handle build artifacts whose build steps can be triggered by more than one condition. There's two ways that happens: 1) the `mtime` (the time of the last modification) of the `.html` is earlier than the `mtime` of the `.md` or 2) a `.published` file exists in the same directory as the `.md` and the `mtime` of the `.published` file is after that of the `.html`. If no `.html` exists, the `stat` function in julia returns `0.0` for the `mtime`, so the file get will be built if it does not exist. The first case is regular rebuilding, the second case is rebuilding for publishing.
 * ./cleanfiles
   * Cleans all .html files in all subdirectories _except ./template.html_.
 * ./convert.jl
   * This file is responsible for generating the HTML out of the various `.md` files, placing it in the template and saving the result as a new `.html`. It also replaces the custom markers in the template with specific values, e.g. `time` preceeded and followed by two `:` gets replaced by a ISO 8601 formatted timestamp, `relative` gets replaced by a certain number of `../` to make relative links resolve properly. Because the replacing of the markers happens after the markdown is converted into HTML, it's possible to use the markers in the markdown. 
 * ./articles/articles.jl
   * This file builds the article overview by checking which subdirectories of `./articles` contain a `.published` file and creates a list of markdown links to the `.html` files. This file is then built using `convert.jl`, just like all the others.

## Conclusio

That's it for now, this should be enough of a reference to follow after not handling this for a number of months. It feels pretty robust and writing this already helped me discover & fix a bug in `buildfiles.jl`, so I consider it a success!