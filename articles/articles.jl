#! /usr/bin/env julia

using Printf

function generateArticleLinks()
    articles = readdir()

    open("articles.md", "w") do outfile
        for article in articles
            !isdir(article) && continue
            !any(==(".published"), readdir(article)) && continue
            
            for f in readdir(article)
                !occursin(r"\.md$", f) && continue
                s = " * [$article]($article/$(replace(f, r"\.md$" => ".html")))\n"
                println(outfile, s)
            end
        end
    end
end

generateArticleLinks()