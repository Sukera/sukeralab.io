.PHONY: articles all clean

all:
	@echo "Goals: articles, clean"

articles:
	cd articles && ./articles.jl
	./convert.jl $(shell ./buildfiles.jl)

clean:
	./cleanfiles.jl