This website is currently being hosted using GitLab Pages. They may or may not serve cookies as part of that hosting.

For more information about this, check their [Privacy Policy](https://about.gitlab.com/privacy/).