#! /usr/bin/env julia

using Dates

if length(ARGS) == 0
    println("pass files to work on as arguments")
    exit(1)
end

using Markdown

function convertFiles()
for file in ARGS
    numDirs = count(x -> x == '/', file) - 1
    relString = "../"^numDirs
    open(replace(file, r"\.md$" => ".html"), "w") do htmlFile
        template = read("./template.html", String)
        buffer = IOBuffer(write = true)
        show(buffer, MIME("text/html"), Markdown.parse_file(file))

        output = replace(template, "<!-- CONTENT HERE -->" => String(take!(buffer)))
        output = replace(output, "::relative::" => relString)

        oldDir = pwd()
        (file ∉ readdir()) && cd(dirname(file))
        if isfile(".published")
            timeString = string(Dates.unix2datetime(stat(".published").mtime))
        else
            timeString = ""
        end
        cd(oldDir)
        output = replace(output, "::time::" => timeString)

        title = basename(dirname(file))
        if title == "."
            title = basename(file)[1:end-3]
        end
        output = replace(output, "::title::" => title)
        write(htmlFile, output)
    end
end
end

convertFiles()