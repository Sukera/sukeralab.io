### Welcome! This is where I write about things.

 * [Articles](::relative::articles/articles.html)
 * [Projects](::relative::projects/projects.html)
 * [About](::relative::about.html)
